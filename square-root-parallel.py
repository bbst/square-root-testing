import math
from assertpy import assert_that
from multiprocessing import Pool
import time

max = 4294967295
no_processes = 10


def calculate_square_root(i):
    square_root = math.sqrt(i)
    assert_that(square_root * square_root).described_as(
        "square root of " + str(i) + " at power of 2 should be " + str(i)).is_close_to(i, 0.001)


range_max = max
range_min = 1
chunks = max // 1000
total_rounds = max // chunks
start_time = time.time()

while range_min > 0:
    range_min = range_max - chunks
    if range_min < 0: range_min = 0
    with Pool(no_processes) as p:
        p.map(calculate_square_root, reversed(range(range_min, range_max + 1)))
    range_max = range_min
    percentage_complete = range_max // chunks * 100 / total_rounds
    current_time = time.time()
    elapsed_time = round((current_time - start_time) / 60, 3)
    estimated_total_time = round(elapsed_time * 100 / (100 - percentage_complete), 3)
    estimated_time_left = round(estimated_total_time - elapsed_time, 3)
    print("still left: " + str(
        round(percentage_complete, 2)) + "%, total time so far: " + str(
        elapsed_time) + " mins, estimated time left: " + str(
        estimated_time_left) + " mins, estimated total time: " + str(estimated_total_time) + " mins")