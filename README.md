# Square Root - Function Testing

This is a simple Python3 script to check that the `sqrt` function from the `math` module works correctly. 

The script will:
  * take every value between 0 and 4294967295 (MAXINT)
  * calculate `sqrt` for the value
  * multiply the result of `sqrt` to itself
  * assert that the result of the multiplication is equal to the original value 

The script can be run using multiple processes, and those are configurable using the `no_processes` variable. By default, the script will run with 10 different processes in parallel. 

# Running the script:

1. Install `assertpy` library using the command: `pip install assertpy` 
2. Run the test using the command: `python square-root-parallel.py`


# Results

The script was tested on a MacBook Pro (15-inch, 2017), 2.9 GHz Quad-Core Intel Core i7, 16GB or RAM, 4 cores. 

With 4 processes in parallel, testing all the values took 103 minutes and 37secs. 

With 10 processes in parallel, testing all the values took  98 minutes and 41secs. 
